import Vue from "vue";
import App from "./App.vue";
import router from "./router/router";
import store from "./store/store";
import vantui from "vant";
import "vant/lib/index.css";
import plugins from "./plugins/extend";
import jQuery from "jquery";
import { Lazyload } from "vant";
import animate from "animate.css";
import api from "./plugins/api";
import echarts from "echarts";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
NProgress.configure({ showSpinner: false, speed: 2000 });
import VueI18n from "vue-i18n";
Vue.use(VueI18n);

const i18n = new VueI18n({
    locale: window.localStorage.getItem("locale") || "zh",
    messages: {
        zh: require("@/assets/lang/zh.js"),
        en: require("@/assets/lang/en.js"),
    },
    silentTranslationWarn: true,
});

Vue.use(animate);

Vue.use(vantui);

// 注册时可以配置额外的选项
Vue.use(Lazyload, {
    lazyComponent: true,
});
Vue.prototype.$query = window.jQuery = jQuery;
Vue.prototype.$echarts = echarts;

/* import "amfe-flexible/index.js";
 */
Vue.prototype.$Plugins = plugins;

Vue.config.productionTip = false;

Vue.$api = Vue.prototype.$api = api;

typeof window.wx !== "undefined" ?
    window.wx.miniProgram.getEnv(function(res) {
        Vue.prototype.$isMPWeiXin = res.miniprogram;
    }) :
    (Vue.prototype.$isMPWeiXin = false);

let ua = window.navigator.userAgent.toLocaleLowerCase();
Vue.prototype.$isWeiXin = /miniprogram/.test(ua);
Vue.prototype.$isMPWeiXin = /micromessenger/.test(ua);

Vue.prototype.$NProgress = NProgress;

plugins.setFontSize(16);

window.addEventListener("resize", () => {
    plugins.setFontSize(16);
});

router.beforeEach((to, from, next) => {
    console.log(to, from);
    to.meta && to.meta.title && jQuery("head>title").html(to.meta.title);
    NProgress.start();
    next();
});

router.afterEach((to, from) => {
    console.log(to, from);
});

new Vue({
    router,
    store,
    i18n,
    NProgress,
    render: (h) => h(App),
}).$mount("#app");