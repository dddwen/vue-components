import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import { test } from "../store/modules/test.js";

const store = new Vuex.Store({
    modules: {
        test,
    },
});

export default store;