export const statesMe = {
    testMeData: {
        me: [{
                name: "userInfo",
                data: {
                    info: {
                        uid: "123456",
                        nickName: "yandong",
                        headImage: "",
                        desc: "个人作者",
                        url: "",
                    },
                    aside: {
                        data: [{
                                count: 1,
                                title: "创作",
                                router: "",
                                params: "{}",
                            },
                            {
                                count: 1,
                                title: "关注",
                                router: "",
                                params: "{}",
                            },
                            {
                                count: 1,
                                title: "收藏",
                                router: "",
                                params: "{}",
                            },
                            {
                                count: 1,
                                title: "浏览",
                                router: "",
                                params: "{}",
                            },
                        ],
                    },
                },
            },
            {
                name: "singleTaskBanner",
                type: "singleTask",
                data: {
                    url: "",
                    image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                    title: "banner test",
                    desc: "dese test",
                    buttonText: "buy $9999",
                    buttonStyle: "background-color:#f21170;color:#fff",
                },
            },
            {
                name: "couponCard",
                data: [{
                        backgroundImage: "url(https://img.zcool.cn/community/016ef65847cf1fa801219c777af739.jpg@1280w_1l_2o_100sh.jpg) no-repeat top left",
                        uid: "123456",
                        nickName: "yandong",
                        headImage: "",
                        desc: "个人作者",
                        url: "",
                        buttons: [{
                                text: "加入会员",
                                url: "",
                            },
                            {
                                text: "领券",
                                url: "",
                            },
                        ],
                    },
                    {
                        backgroundImage: "url(https://img.zcool.cn/community/016ef65847cf1fa801219c777af739.jpg@1280w_1l_2o_100sh.jpg) no-repeat top left",
                        uid: "123456",
                        nickName: "yandong",
                        headImage: "",
                        desc: "个人作者",
                        url: "",
                        buttons: [{
                                text: "加入会员",
                                url: "",
                            },
                            {
                                text: "领券",
                                url: "",
                            },
                        ],
                    },
                ],
            },
            {
                name: "icons",
                rowCount: 2,
                offset: 6,
                data: [{
                        url: "",
                        icon: "smile-o",
                        title: "test",
                    },
                    {
                        url: "",
                        icon: "smile-o",
                        title: "test",
                    },
                    {
                        url: "",
                        icon: "smile-o",
                        title: "test",
                    },
                    {
                        url: "",
                        icon: "smile-o",
                        title: "test",
                    },
                    {
                        url: "",
                        icon: "smile-o",
                        title: "test",
                    },
                    {
                        url: "",
                        icon: "smile-o",
                        title: "test",
                    },
                    {
                        url: "",
                        icon: "smile-o",
                        title: "test",
                    },
                    {
                        url: "",
                        icon: "smile-o",
                        title: "test",
                    },
                ],
            },
            {
                name: "cardframe",
                title: "card1",
                data: {
                    symmetryTaskBanner: {
                        type: "symmetryTask",
                        name: "symmetryTaskBanner",
                        data: [{
                                desc: "昨日阅读数",
                                title: "计算中",
                                asideTitle: "较昨日",
                                asideData: "--",
                                url: "",
                            },
                            {
                                desc: "昨日阅读数",
                                title: "计算中",
                                asideTitle: "较昨日",
                                asideData: "--",
                                url: "",
                            },
                        ],
                    },
                },
            },
            {
                name: "cardframe",
                data: {
                    swipeNoTask: {
                        name: "swipeNoTask",
                        type: "swipeNoTask",
                        width: "270",
                        height: "60",
                        showDock: false,
                        speed: 4000,
                        showBackground: false,
                        isTask: true,
                        data: [{
                                type: "singleNoTask",
                                data: [{
                                    url: "",
                                    image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                                    title: "相关数据取得，可以看到的",
                                }, ],
                            },
                            {
                                type: "singleNoTask",
                                data: [{
                                    url: "",
                                    image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                                    title: "相关数据取得，可以看到的",
                                }, ],
                            },
                            {
                                type: "singleNoTask",
                                data: [{
                                    url: "",
                                    image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                                    title: "相关数据取得，可以看到的",
                                }, ],
                            },
                            {
                                type: "singleNoTask",
                                data: [{
                                    url: "",
                                    image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                                    title: "相关数据取得，可以看到的",
                                }, ],
                            },
                            {
                                type: "singleNoTask",
                                data: [{
                                    url: "",
                                    image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                                    title: "相关数据取得，可以看到的",
                                }, ],
                            },
                        ],
                    },
                },
            },
            {
                name: "cardframe",
                data: {
                    swipeNoTask: {
                        name: "swipeNoTask",
                        type: "swipeNoTask",
                        width: "270",
                        height: "60",
                        showDock: false,
                        speed: 3000,
                        showBackground: false,
                        isTask: true,
                        data: [{
                                type: "singleNoTask",
                                data: [{
                                    url: "",
                                    image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                                    title: "相关数据取得，可以看到的",
                                }, ],
                            },
                            {
                                type: "singleNoTask",
                                data: [{
                                    url: "",
                                    image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                                    title: "相关数据取得，可以看到的",
                                }, ],
                            },
                            {
                                type: "singleNoTask",
                                data: [{
                                    url: "",
                                    image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                                    title: "相关数据取得，可以看到的",
                                }, ],
                            },
                            {
                                type: "singleNoTask",
                                data: [{
                                    url: "",
                                    image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                                    title: "相关数据取得，可以看到的",
                                }, ],
                            },
                            {
                                type: "singleNoTask",
                                data: [{
                                    url: "",
                                    image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                                    title: "相关数据取得，可以看到的",
                                }, ],
                            },
                        ],
                    },
                },
            },
            {
                name: "cardframe",
                data: {
                    swipeNoTask: {
                        name: "swipeNoTask",
                        type: "swipeNoTask",
                        width: "270",
                        height: "60",
                        showDock: false,
                        speed: 2000,
                        showBackground: false,
                        isTask: true,
                        data: [{
                                type: "singleNoTask",
                                data: [{
                                    url: "",
                                    image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                                    title: "相关数据取得，可以看到的",
                                }, ],
                            },
                            {
                                type: "singleNoTask",
                                data: [{
                                    url: "",
                                    image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                                    title: "相关数据取得，可以看到的",
                                }, ],
                            },
                            {
                                type: "singleNoTask",
                                data: [{
                                    url: "",
                                    image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                                    title: "相关数据取得，可以看到的",
                                }, ],
                            },
                            {
                                type: "singleNoTask",
                                data: [{
                                    url: "",
                                    image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                                    title: "相关数据取得，可以看到的",
                                }, ],
                            },
                            {
                                type: "singleNoTask",
                                data: [{
                                    url: "",
                                    image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                                    title: "相关数据取得，可以看到的",
                                }, ],
                            },
                        ],
                    },
                },
            },
            {
                name: "floatBanner",
                type: "float",
                direction: "left",
                data: [{
                    image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                    title: "左侧悬浮广告测试",
                    desc: "详细介绍更多内容，详细介绍更多内容，详细介绍更多内容",
                    url: "",
                }, ],
            },
            {
                name: "floatBanner",
                type: "float",
                direction: "right",
                data: [{
                    image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                    title: "右侧悬浮广告测试",
                    desc: "详细介绍更多内容，详细介绍更多内容，详细介绍更多内容",
                    url: "",
                }, ],
            },
        ],
    },
};