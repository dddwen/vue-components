export const statesVideo = {
    testVideoListData: {
        video: [{
                name: "unsymmetricLeft",
                type: "unsymmetricLeft",
                title: "广告",
                slogan: "test",
                data: [{
                        url: "",
                        image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                        title: "banner test",
                        desc: "dese test",
                    },
                    {
                        url: "",
                        image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                        title: "banner test",
                        desc: "dese test",
                    },
                    {
                        url: "",
                        image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                        title: "banner test",
                        desc: "dese test",
                    },
                ],
            },
            {
                name: "unsymmetricRight",
                type: "unsymmetricRight",
                title: "广告",
                slogan: "test",
                data: [{
                        url: "",
                        image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                        title: "banner test",
                        desc: "dese test",
                    },
                    {
                        url: "",
                        image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                        title: "banner test",
                        desc: "dese test",
                    },
                    {
                        url: "",
                        image: "https://img.zcool.cn/community/01f1485847ce2ea8012060c8a7823e.jpg@1280w_1l_2o_100sh.jpg",
                        title: "banner test",
                        desc: "dese test",
                    },
                ],
            },
            {
                name: "echarts",
                data: [{
                        width: "auto",
                        height: 320,
                    },
                    {
                        width: "auto",
                        height: 220,
                    },
                ],
            },
            {
                name: "videoList",
                data: [{
                        image: "https://i.keaitupian.net/up/9d/b4/b1/183523a1b1ac6cbfe3259420a8b1b49d.jpeg",
                        router: "videoPlayer",
                        title: "增强版的 img 标签，提供多种图片填充模式，支持图片懒加载",
                        superscript: "live",
                        aside: {
                            playCount: 333,
                            good: 120,
                        },
                    },
                    {
                        image: "https://www.wahaotu.com/uploads/allimg/202009/1599981807792942.jpg",
                        router: "videoPlayer",
                        title: "增强版的 img 标签，提供多种图片填充模式，支持图片懒加载",
                        aside: {
                            playCount: 333,
                            good: 120,
                        },
                    },
                    {
                        image: "https://i.keaitupian.net/up/9d/b4/b1/183523a1b1ac6cbfe3259420a8b1b49d.jpeg",
                        router: "videoPlayer",
                        title: "增强版的 img 标签，提供多种图片填充模式，支持图片懒加载",
                        aside: {
                            playCount: 333,
                            good: 120,
                        },
                    },
                    {
                        image: "https://www.wahaotu.com/uploads/allimg/202009/1599981807792942.jpg",
                        router: "videoPlayer",
                        title: "增强版的 img 标签，提供多种图片填充模式，支持图片懒加载",
                        aside: {
                            playCount: 333,
                            good: 120,
                        },
                    },
                    {
                        image: "https://i.keaitupian.net/up/9d/b4/b1/183523a1b1ac6cbfe3259420a8b1b49d.jpeg",
                        router: "videoPlayer",
                        title: "增强版的 img 标签，提供多种图片填充模式，支持图片懒加载",
                        aside: {
                            playCount: 333,
                            good: 120,
                        },
                    },
                    {
                        image: "https://www.wahaotu.com/uploads/allimg/202009/1599981807792942.jpg",
                        router: "videoPlayer",
                        title: "增强版的 img 标签，提供多种图片填充模式，支持图片懒加载",
                        aside: {
                            playCount: 333,
                            good: 120,
                        },
                    },
                    {
                        image: "https://i.keaitupian.net/up/9d/b4/b1/183523a1b1ac6cbfe3259420a8b1b49d.jpeg",
                        router: "videoPlayer",
                        title: "增强版的 img 标签，提供多种图片填充模式，支持图片懒加载",
                        aside: {
                            playCount: 333,
                            good: 120,
                        },
                    },
                    {
                        image: "https://www.wahaotu.com/uploads/allimg/202009/1599981807792942.jpg",
                        router: "videoPlayer",
                        title: "增强版的 img 标签，提供多种图片填充模式，支持图片懒加载",
                        aside: {
                            playCount: 333,
                            good: 120,
                        },
                    },
                    {
                        image: "https://i.keaitupian.net/up/9d/b4/b1/183523a1b1ac6cbfe3259420a8b1b49d.jpeg",
                        router: "videoPlayer",
                        title: "增强版的 img 标签，提供多种图片填充模式，支持图片懒加载",
                        aside: {
                            playCount: 333,
                            good: 120,
                        },
                    },
                    {
                        image: "https://www.wahaotu.com/uploads/allimg/202009/1599981807792942.jpg",
                        router: "",
                        title: "增强版的 img 标签，提供多种图片填充模式，支持图片懒加载",
                        aside: {
                            playCount: 333,
                            good: 120,
                        },
                    },
                    {
                        image: "https://i.keaitupian.net/up/9d/b4/b1/183523a1b1ac6cbfe3259420a8b1b49d.jpeg",
                        router: "",
                        title: "增强版的 img 标签，提供多种图片填充模式，支持图片懒加载",
                        aside: {
                            playCount: 333,
                            good: 120,
                        },
                    },
                    {
                        image: "https://www.wahaotu.com/uploads/allimg/202009/1599981807792942.jpg",
                        router: "",
                        title: "增强版的 img 标签，提供多种图片填充模式，支持图片懒加载",
                        aside: {
                            playCount: 333,
                            good: 120,
                        },
                    },
                    {
                        image: "https://i.keaitupian.net/up/9d/b4/b1/183523a1b1ac6cbfe3259420a8b1b49d.jpeg",
                        router: "",
                        title: "增强版的 img 标签，提供多种图片填充模式，支持图片懒加载",
                        aside: {
                            playCount: 333,
                            good: 120,
                        },
                    },
                    {
                        image: "https://www.wahaotu.com/uploads/allimg/202009/1599981807792942.jpg",
                        router: "",
                        title: "增强版的 img 标签，提供多种图片填充模式，支持图片懒加载",
                        aside: {
                            playCount: 333,
                            good: 120,
                        },
                    },
                ],
            },
        ],
    },
};