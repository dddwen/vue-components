export const statesVideoPlayer = {
    testVideoPlayerData: {
        videoPlayer: [{
            name: "videoView",
            muted: true,
            preload: true,
            loop: true,
            controls: true,
            autoplay: false,
            data: [{
                    src: "//vjs.zencdn.net/v/oceans.mp4",
                    poster: "//vjs.zencdn.net/v/oceans.png",
                    title: "Vue完整教程:第15讲,结合webpack实现children子路由",
                    desc: "视频内容简介...",
                },
                {
                    src: "//vjs.zencdn.net/v/oceans.mp4",
                    poster: "//vjs.zencdn.net/v/oceans.png",
                    title: "Vue完整教程:第15讲,结合webpack实现children子路由",
                    desc: "视频内容简介...",
                },
                {
                    src: "//vjs.zencdn.net/v/oceans.mp4",
                    poster: "//vjs.zencdn.net/v/oceans.png",
                    title: "Vue完整教程:第15讲,结合webpack实现children子路由",
                    desc: "视频内容简介...",
                },
                {
                    src: "//vjs.zencdn.net/v/oceans.mp4",
                    poster: "//vjs.zencdn.net/v/oceans.png",
                    title: "Vue完整教程:第15讲,结合webpack实现children子路由",
                    desc: "视频内容简介...",
                },
                {
                    src: "//vjs.zencdn.net/v/oceans.mp4",
                    poster: "//vjs.zencdn.net/v/oceans.png",
                    title: "Vue完整教程:第15讲,结合webpack实现children子路由",
                    desc: "视频内容简介...",
                },
            ],
        }, ],
    },
};