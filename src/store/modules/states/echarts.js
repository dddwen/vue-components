export const statesEcharts = {
    testEchartsOptionData: {
        data: [{
                title: {
                    text: "ECharts 实例1",
                    textStyle: {
                        fontSize: 12,
                    },
                },
                tooltip: {},
                legend: {
                    data: ["销量"],
                },
                xAxis: {
                    data: ["衬衫", "羊毛衫", "雪纺衫", "裤子", "高跟鞋", "袜子"],
                },
                yAxis: {},
                series: [{
                    name: "销量",
                    type: "bar",
                    data: [5, 20, 36, 10, 10, 20],
                }, ],
            },
            {
                title: {
                    text: "ECharts 实例2",
                    textStyle: {
                        fontSize: 12,
                    },
                },
                tooltip: {},
                legend: {
                    data: ["销量"],
                },
                xAxis: {
                    data: ["衬衫", "羊毛衫", "雪纺衫", "裤子", "高跟鞋", "袜子"],
                },
                yAxis: {},
                series: [{
                    name: "销量",
                    type: "bar",
                    data: [5, 20, 36, 10, 10, 20],
                }, ],
            },
        ],
    },
};