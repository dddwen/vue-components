import { actionsTest } from "../../store/modules/actions/test";
import { statesTest } from "../../store/modules/states/test";
import { statesDiscover } from "../../store/modules/states/discover";
import { statesVideo } from "../../store/modules/states/video";
import { statesEcharts } from "../../store/modules/states/echarts";
import { statesCollect } from "../../store/modules/states/collect";
import { statesMe } from "../../store/modules/states/me";
import { statesVideoPlayer } from "../../store/modules/states/videoPlayer";
import { mutationsTest } from "../../store/modules/mutations/test";

export const test = {
    getters: {},
    //提交数据到state
    mutations: {...mutationsTest },
    //通过处理后再提交mutations到state
    actions: {...actionsTest },
    state: {
        ...statesTest,
        ...statesDiscover,
        ...statesVideo,
        ...statesEcharts,
        ...statesCollect,
        ...statesMe,
        ...statesVideoPlayer,
    },
};