export const actionsTest = {
    test({ state }, key) {
        return new Promise((resolve, reject) => {
            try {
                //console.log(this._vm.$api, commit);
                resolve(state.testData[key]);
            } catch (e) {
                reject(e);
            }
        });
    },
    testHomeList({ state }, key) {
        return new Promise((resolve, reject) => {
            try {
                //console.log(this._vm.$api, commit);
                resolve(state.testHomeListData[key]);
            } catch (e) {
                reject(e);
            }
        });
    },
    getStateChannelData({ state }) {
        return new Promise((resolve, reject) => {
            try {
                resolve(state.testChannelData);
            } catch (e) {
                reject(e);
            }
        });
    },
    getMoreStateData({ state }, api, args) {
        console.log(args, state);
        return new Promise((resolve, reject) => {
            try {
                this._vm.$api
                    .post("/")
                    .then(() => {
                        setTimeout(function() {
                            resolve(state.moreData[api]);
                        }, 5000);
                    })
                    .catch(function(v) {
                        reject(v);
                    });
            } catch (e) {
                reject(e);
            }
        });
    },
    getStatePageItemsData({ state }, name) {
        console.log(name);
        return new Promise((resolve, reject) => {
            try {
                //console.log(this._vm.$api, commit);
                resolve(state.testDiscoverListData[name]);
            } catch (e) {
                reject(e);
            }
        });
    },
    getStatePageVideoItemsData({ state }, name) {
        console.log(name);
        return new Promise((resolve, reject) => {
            try {
                //console.log(this._vm.$api, commit);
                resolve(state.testVideoListData[name]);
            } catch (e) {
                reject(e);
            }
        });
    },
    getStateEchartsData({ state }, name) {
        console.log(name);
        return new Promise((resolve, reject) => {
            try {
                //console.log(this._vm.$api, commit);
                resolve(state.testEchartsOptionData.data);
            } catch (e) {
                reject(e);
            }
        });
    },
    getStateCollectData({ state }, name) {
        console.log(name);
        return new Promise((resolve, reject) => {
            try {
                //console.log(this._vm.$api, commit);
                resolve(state.testCollectData[name]);
            } catch (e) {
                reject(e);
            }
        });
    },
    getStateMeData({ state }, name) {
        console.log(name);
        return new Promise((resolve, reject) => {
            try {
                //console.log(this._vm.$api, commit);
                resolve(state.testMeData[name]);
            } catch (e) {
                reject(e);
            }
        });
    },
    getStatePageVideoPlayerItemsData({ state }, name) {
        console.log(name);
        return new Promise((resolve, reject) => {
            try {
                //console.log(this._vm.$api, commit);
                resolve(state.testVideoPlayerData[name]);
            } catch (e) {
                reject(e);
            }
        });
    },
};