import Router from "vue-router";
import Vue from "vue";

Vue.use(Router);

let router = new Router({
    routes: [{
            path: "/",
            name: "home",
            component: () =>
                import ( /* webpackChunkName: 'home' */ "../views/home"),
            meta: {
                title: "推荐",
            },
        },
        {
            path: "/discover",
            name: "discover",
            component: () =>
                import ( /* webpackChunkName: 'discover' */ "../views/discover"),
            meta: {
                title: "发现",
            },
        },
        {
            path: "/video",
            name: "video",
            component: () =>
                import ( /* webpackChunkName: 'video' */ "../views/video"),
            meta: {
                title: "视频",
            },
        },
        {
            path: "/video/player",
            name: "videoPlayer",
            component: () =>
                import (
                    /* webpackChunkName:'vidoeplayer' */
                    "../views/video/videoplayer"
                ),
            meta: {
                title: "视频播放",
            },
        },
        {
            path: "/collect",
            name: "collect",
            component: () =>
                import ( /* webpackChunkName: 'collect' */ "../views/collect"),
            meta: {
                title: "收藏",
            },
        },
        {
            path: "/me",
            name: "me",
            component: () =>
                import ( /* webpackChunkName: 'me' */ "../views/me"),
            meta: {
                title: "我的",
            },
        },
    ],
});

export default router;