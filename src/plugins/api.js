import axios from "axios";
import { Notify } from "vant";

//创建axios实例
const requests = axios.create({
    baseURL: process.env.VUE_APP_API, //基础url,如果是多环境配置这样写，也可以像下面一行的写死。 // baseURL: 'http://168.192.0.123',
    timeout: 6000, //请求超时时间
});

// 错误处理函数
const err = (error) => {
    if (error.response) {
        const data = error.response.data;
        if (error.response.status === 403) {
            Notify({ type: "danger", message: data.message || data.msg });
        }
        if (error.response.status === 401) {
            Notify({ type: "danger", message: "你没有权限。" });
        }
    }
    return Promise.reject(error);
};

//(请求拦截器)
requests.interceptors.request.use((config) => {
    const token = localStorage.getItem("token");
    if (token) {
        config.headers["token"] = token; //让每个请求携带自定义token请根据实际情况自行修改
    }
    return config;
}, err);

//（接收拦截器）
requests.interceptors.response.use((response) => {
    const res = response.data;
    if (res.code !== 0 && res.code !== 200) {
        Notify({ type: "danger", message: res.message || res.msg }); //401:未登录;
        if (res.code === 401 || res.code === 403 || res.code === 999) {
            Notify({ type: "danger", message: "请登录" });
        }
        return Promise.reject("error");
    } else {
        return res;
    }
}, err);

export default requests;